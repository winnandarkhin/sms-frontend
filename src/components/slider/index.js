import React from "react"
import { Carousel } from 'antd';
import SliderOne from '../../images/slider1.jpg'
import SliderTwo from '../../images/slider2.jpg'
import SliderThree from '../../images/slider3.jpg'

class Slider extends React.Component{

    onChange(a, b, c) {
        console.log(a, b, c);
    }

    render(){
        return(
            <Carousel afterChange={this.onChange} className="slider">
                 <div>
                   <img alt="slider" src={SliderThree}/>
                </div>
                <div>
                    <img alt="slider" src={SliderOne}/>
                </div>
                <div>
                    <img alt="slider" src={SliderTwo} />
                </div>
            </Carousel>
        )
    }
}

export default Slider;