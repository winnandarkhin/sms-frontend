import React from "react"
import {Link} from "react-router-dom"
class Navbar extends React.Component{
    render(){
        return(
                <header className="header">
                    <a href="/" className="logo">SERVICE MANAGEMENT SYSTEM</a>
                    <input className="menu-btn" type="checkbox" id="menu-btn" />
                    <label className="menu-icon" for="menu-btn"><span className="navicon"></span></label>
                    <ul className="menu">
                        <li><Link to="/" >Home</Link></li>
                        <li><Link to="/about" >About</Link></li>
                        <li><Link to="/news" >News</Link></li>
                        <li><Link to="/service" >Services</Link></li>
                        <li><Link to="/contact" >Contact</Link></li>
                    </ul>
                </header>
        )
    }
}
export default Navbar;